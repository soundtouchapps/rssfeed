package com.galaxiapps.galaxiforzemingo.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.galaxiapps.galaxiforzemingo.R
import com.galaxiapps.galaxiforzemingo.databinding.HomeFragmentBinding
import com.galaxiapps.galaxiforzemingo.rss.feed.RSSFeedViewModel
import com.galaxiapps.galaxiforzemingo.rss.pager.RSSPagerFragment
import java.time.Instant

class HomeFragment : Fragment() {

    lateinit var binding: HomeFragmentBinding
    private val viewModel: RSSFeedViewModel by activityViewModels()

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.goToFeed.setOnClickListener {
            activity?.let {
                val ft = it.supportFragmentManager.beginTransaction()
                val frag = RSSPagerFragment.newInstance()
                ft.replace(R.id.container, frag)
                ft.addToBackStack(null)
                ft.commit()
            }
        }

        viewModel.chosenTitle.observe(viewLifecycleOwner){ lastChosenLabel ->
            binding.lastItem.text = lastChosenLabel
        }
    }


    override fun onResume() {
        super.onResume()
        binding.dateTime.text = UIUtils().getReadableTime(Instant.now().toEpochMilli())
    }


}