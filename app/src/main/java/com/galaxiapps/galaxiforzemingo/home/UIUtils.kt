package com.galaxiapps.galaxiforzemingo.home

import java.text.SimpleDateFormat
import java.util.*

open class UIUtils {

    fun getReadableTime(millis: Long): String{
        return try {
            val sdf = SimpleDateFormat("dd/MM/yyy HH:mm", Locale.getDefault())
            val netDate = Date(millis)
            sdf.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
    }
}