package com.galaxiapps.galaxiforzemingo.rss.data

data class RSSItem(val title: String?, val link: String?)
