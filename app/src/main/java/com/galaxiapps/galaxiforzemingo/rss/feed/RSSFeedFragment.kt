package com.galaxiapps.galaxiforzemingo.rss.feed

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.galaxiapps.galaxiforzemingo.R
import com.galaxiapps.galaxiforzemingo.rss.data.FeedType
import com.galaxiapps.galaxiforzemingo.databinding.RssFeedFragmentBinding
import com.galaxiapps.galaxiforzemingo.rss.data.RSSItem

class RSSFeedFragment : Fragment() {

    private val viewModel: RSSFeedViewModel by activityViewModels()
    private lateinit var binding: RssFeedFragmentBinding
    private var requestedFeed = FeedType.CARS

    companion object {

        private const val REQUESTED_FEED = "feedType"

        fun newInstance(requestedFeed: FeedType): RSSFeedFragment{
            val bundle = Bundle()
            bundle.putSerializable(REQUESTED_FEED, requestedFeed)
            val frag = RSSFeedFragment()
            frag.arguments = bundle

            return frag
        }
    }

    private fun readBundle(bundle: Bundle) {
        requestedFeed = bundle.getSerializable(REQUESTED_FEED) as FeedType
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        readBundle(requireArguments())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = RssFeedFragmentBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.feedRecyclerView.adapter = RSSFeedListAdapter(viewModel, listOf())

        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when(requestedFeed){
            FeedType.CARS -> {
                viewModel.carsList.observe(viewLifecycleOwner){ carsList ->
                    updateCarsAdapter(carsList)
                }
            }
            else ->{
                viewModel.sportsAndCultureList.observe(viewLifecycleOwner){ sportsList ->
                    updateSportsAndCultureAdapter(sportsList)
                }
            }
        }

        //TODO: differentiate between errors for more precise message to user
        viewModel.loadLiveDataError.observe(viewLifecycleOwner){
            Toast.makeText(activity, getString(R.string.fetch_rss_error), Toast.LENGTH_LONG).show()
        }
    }

    override fun onResume() {
        super.onResume()
        when(requestedFeed){
            FeedType.CARS -> viewModel.loadCars()
            else -> viewModel.loadSportsAndCulture()
        }
    }

    //TODO: use diff util to compare changes in list and update accordingly
    private fun updateCarsAdapter(carsList: List<RSSItem>) {
        (binding.feedRecyclerView.adapter as RSSFeedListAdapter).apply {
            itemList = carsList
            notifyDataSetChanged()
        }
    }

    //TODO: use diff util to compare changes in list and update accordingly
    private fun updateSportsAndCultureAdapter(sportsList: List<RSSItem>) {
        (binding.feedRecyclerView.adapter as RSSFeedListAdapter).apply {
            itemList = sportsList
            notifyDataSetChanged()
        }
    }

    override fun onPause() {
        super.onPause()
        when(requestedFeed) {
            FeedType.CARS -> viewModel.stopLoadingCars()
            FeedType.SPORTS_CULTURE -> viewModel.stopLoadingSportsAndCulture()
        }
    }

}