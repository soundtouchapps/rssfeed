package com.galaxiapps.galaxiforzemingo.rss.feed

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.galaxiapps.galaxiforzemingo.databinding.ItemRowBinding
import com.galaxiapps.galaxiforzemingo.rss.data.RSSItem

open class RSSFeedListAdapter(val model: RSSFeedViewModel, var itemList: List<RSSItem>): RecyclerView.Adapter<RSSFeedListAdapter.RSSItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RSSItemViewHolder {
        val itemBinding = ItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RSSItemViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RSSItemViewHolder, position: Int) {
        itemList[position].let { holder.bind(it) }
    }

    inner class RSSItemViewHolder(private val itemBinding: ItemRowBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(item: RSSItem){
            itemBinding.articleTitle.text = item.title
            itemBinding.articleTitle.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(item.link))
                itemBinding.articleTitle.context.startActivity(intent)

                item.title?.let { it1 -> model.onTitleClicked(title = it1) }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}

