package com.galaxiapps.galaxiforzemingo.rss.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.galaxiapps.galaxiforzemingo.rss.network.RSSServiceImpl
import com.galaxiapps.galaxiforzemingo.rss.repository.RSSRepositoryImpl
import com.galaxiapps.galaxiforzemingo.rss.repository.URL_RSS_CARS
import com.galaxiapps.galaxiforzemingo.rss.repository.URL_RSS_CULTURE
import com.galaxiapps.galaxiforzemingo.rss.repository.URL_RSS_SPORT
import com.galaxiapps.galaxiforzemingo.rss.data.RSSItem
import com.galaxiapps.galaxiforzemingo.rss.utils.combineLatest
//import com.snakydesign.livedataextensions.combineLatest
import kotlinx.coroutines.*
import java.lang.Exception
import java.util.concurrent.TimeUnit

class RSSFeedViewModel : ViewModel() {

    private var rssRepository =  RSSRepositoryImpl(RSSServiceImpl())

    private val _loadLiveDataError = MutableLiveData<Throwable>()
    private val _chosenTitle = MutableLiveData<String>()
    private val _isRSSFeedLoading = MutableLiveData<Boolean>()

    private val _carsList = MutableLiveData<List<RSSItem>>()
    private val _sportsList = MutableLiveData<List<RSSItem>>()
    private val _cultureList = MutableLiveData<List<RSSItem>>()

    val loadLiveDataError : LiveData<Throwable> get() = _loadLiveDataError
    val chosenTitle : LiveData<String> get() = _chosenTitle
    val isRSSFeedLoading : LiveData<Boolean> get() = _isRSSFeedLoading

    val carsList : LiveData<List<RSSItem>> get() = _carsList
    val sportsAndCultureList : LiveData<List<RSSItem>> get() =
        combineLatest(_sportsList, _cultureList) { sportList, cultureList -> sportList.orEmpty().plus(cultureList.orEmpty()) }

    private var carJob: Job? = null
    private var sportsJob: Job? = null
    private var cultureJob: Job? = null


    fun onTitleClicked(title: String){
        _chosenTitle.postValue(title)
    }

    private suspend fun startCoroutineTimer(delayMillis: Long = 0, repeatMillis: Long = 0, action: suspend () -> Unit){
        delay(delayMillis)
        if (repeatMillis > 0) {
            while (true) {
                action()
                delay(repeatMillis)
            }
        } else {
            action()
        }
    }

    fun loadCars() {
       carJob = viewModelScope.launch(Dispatchers.IO) {
           startCoroutineTimer(0, TimeUnit.SECONDS.toMillis(5), suspend {
                _isRSSFeedLoading.postValue(true)
                try {
                    val result = rssRepository.loadRSS(URL_RSS_CARS)
                    _carsList.postValue(result)
                }catch (ex: Exception){
                    _loadLiveDataError.postValue(ex)
                } finally {
                    _isRSSFeedLoading.postValue(false)
                }
            })
        }
    }

    fun stopLoadingCars(){
        carJob?.cancel()
        carJob = null
    }

    fun loadSportsAndCulture(){
        loadSports()
        loadCulture()
    }

    private fun loadSports(){
        sportsJob = viewModelScope.launch (Dispatchers.IO){
            startCoroutineTimer(0, TimeUnit.SECONDS.toMillis(5), suspend {
                _isRSSFeedLoading.postValue(true)
                try {
                    val result = rssRepository.loadRSS(URL_RSS_SPORT)
                    _sportsList.postValue(result)
                }catch (ex: Exception){
                    _loadLiveDataError.postValue(ex)
                } finally {
                    _isRSSFeedLoading.postValue(false)
                }
            })
        }
    }


    private fun loadCulture(){
        cultureJob = viewModelScope.launch (Dispatchers.IO){
            startCoroutineTimer(0, TimeUnit.SECONDS.toMillis(5), suspend {
                _isRSSFeedLoading.postValue(true)
                try {
                    val result = rssRepository.loadRSS(URL_RSS_CULTURE)
                    _cultureList.postValue(result)
                }catch (ex: Exception){
                    _loadLiveDataError.postValue(ex)
                } finally {
                    _isRSSFeedLoading.postValue(false)
                }
            })
        }
    }

    fun stopLoadingSportsAndCulture(){
        sportsJob?.cancel()
        sportsJob = null
        cultureJob?.cancel()
        cultureJob = null
    }


}