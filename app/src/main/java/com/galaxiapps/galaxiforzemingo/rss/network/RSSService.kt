package com.galaxiapps.galaxiforzemingo.rss.network

import java.io.InputStream

interface RSSService {

    fun downloadRSS(listURL: String): InputStream?
}