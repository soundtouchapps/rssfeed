package com.galaxiapps.galaxiforzemingo.rss.network

import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class RSSServiceImpl: RSSService {

    @Throws(IOException::class)
    override fun downloadRSS(listURL: String): InputStream? {
        val url = URL(listURL)
        return (url.openConnection() as? HttpURLConnection)?.run {
            readTimeout = 10000
            connectTimeout = 15000
            requestMethod = "GET"
            doInput = true
            connect()
            inputStream
        }
    }
}