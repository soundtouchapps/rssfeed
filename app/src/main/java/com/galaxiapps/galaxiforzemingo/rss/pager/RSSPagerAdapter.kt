package com.galaxiapps.galaxiforzemingo.rss.pager

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.galaxiapps.galaxiforzemingo.rss.data.FeedType
import com.galaxiapps.galaxiforzemingo.rss.feed.RSSFeedFragment


class RSSPagerAdapter(private val tabTitles: Array<Int>, fragment: Fragment) :
    FragmentStateAdapter(fragment) {


    override fun createFragment(position: Int): Fragment {
        return when (position){
            0 -> RSSFeedFragment.newInstance(FeedType.CARS)
            else -> RSSFeedFragment.newInstance(FeedType.SPORTS_CULTURE)
        }
    }


    override fun getItemCount(): Int {
        return tabTitles.size
    }

}