package com.galaxiapps.galaxiforzemingo.rss.pager

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.galaxiapps.galaxiforzemingo.R
import com.galaxiapps.galaxiforzemingo.databinding.FragmentRssBinding
import com.galaxiapps.galaxiforzemingo.rss.feed.RSSFeedViewModel
import com.google.android.material.tabs.TabLayoutMediator

private val TAB_TITLES = arrayOf(
    R.string.car_title,
    R.string.sport_culture_title
)

class RSSPagerFragment : Fragment() {

    private val viewModel: RSSFeedViewModel by activityViewModels()
    private lateinit var binding: FragmentRssBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRssBinding.inflate(inflater, container, false)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewPager.adapter = RSSPagerAdapter(TAB_TITLES, this)

        TabLayoutMediator(binding.tabLayout, binding.viewPager){ tab, position ->
            tab.text = when(position){
                0 -> getString(TAB_TITLES[0])
                else -> getString(TAB_TITLES[1])
            }
        }.attach()

        viewModel.isRSSFeedLoading.observe(viewLifecycleOwner){
                isLoading ->
            binding.feedProgress.visibility = if(isLoading) View.VISIBLE else View.GONE
        }
    }

    companion object {
        fun newInstance() = RSSPagerFragment()
    }
}