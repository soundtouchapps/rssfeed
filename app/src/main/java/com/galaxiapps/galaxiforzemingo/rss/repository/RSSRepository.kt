package com.galaxiapps.galaxiforzemingo.rss.repository

import com.galaxiapps.galaxiforzemingo.rss.data.RSSItem

interface RSSRepository {

    suspend fun loadRSS(url: String): List<RSSItem>

}