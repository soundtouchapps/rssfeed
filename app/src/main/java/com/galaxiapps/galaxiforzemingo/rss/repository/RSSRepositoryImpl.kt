package com.galaxiapps.galaxiforzemingo.rss.repository

import com.galaxiapps.galaxiforzemingo.rss.network.RSSServiceImpl
import com.galaxiapps.galaxiforzemingo.rss.data.RSSItem
import com.galaxiapps.galaxiforzemingo.rss.utils.XMLParser

const val URL_RSS_CARS = "http://www.ynet.co.il/Integration/StoryRss550.xml"
const val URL_RSS_SPORT = "http://www.ynet.co.il/Integration/StoryRss3.xml"
const val URL_RSS_CULTURE = "http://www.ynet.co.il/Integration/StoryRss538.xml"
class RSSRepositoryImpl(private val rssServiceImpl: RSSServiceImpl): RSSRepository {


    override suspend fun loadRSS(url: String): List<RSSItem> {
        rssServiceImpl.downloadRSS(url).use { stream ->
            return if (stream != null) {
                XMLParser().parse(stream)
            }else{
                emptyList()
            }
        }
    }

}