package com.galaxiapps.galaxiforzemingo.rss.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

/**
 * Created by Adib Faramarzi
 * from https://github.com/adibfara/Lives
 */

fun <X, Y, R> combineLatest(first: LiveData<X>, second: LiveData<Y>, combineFunction: (X?, Y?) -> R): LiveData<R> {
    val finalLiveData: MediatorLiveData<R> = MediatorLiveData()

    val firstEmit: Emit<X?> = Emit()
    val secondEmit: Emit<Y?> = Emit()

    val combine: () -> Unit = {
        if (firstEmit.emitted && secondEmit.emitted) {
            val combined = combineFunction(firstEmit.value, secondEmit.value)
            finalLiveData.value = combined
        }
    }

    finalLiveData.addSource(first) { value ->
        firstEmit.value = value
        combine()
    }
    finalLiveData.addSource(second) { value ->
        secondEmit.value = value
        combine()
    }
    return finalLiveData
}


/**
 * Combines the latest values from multiple LiveData objects.
 * First emits after all LiveData objects have emitted a value, and will emit afterwards after any
 * of them emits a new value.
 *
 * The difference between combineLatest and zip is that the zip only emits after all LiveData
 * objects have a new value, but combineLatest will emit after any of them has a new value.
 */
fun <X, Y, Z, R> combineLatest(
    first: LiveData<X>,
    second: LiveData<Y>,
    third: LiveData<Z>,
    combineFunction: (X?, Y?, Z?) -> R
): LiveData<R> {
    val finalLiveData: MediatorLiveData<R> = MediatorLiveData()

    val firstEmit: Emit<X?> = Emit()
    val secondEmit: Emit<Y?> = Emit()
    val thirdEmit: Emit<Z?> = Emit()

    val combine: () -> Unit = {
        if (firstEmit.emitted && secondEmit.emitted && thirdEmit.emitted) {
            val combined = combineFunction(firstEmit.value, secondEmit.value, thirdEmit.value)
            finalLiveData.value = combined
        }
    }

    finalLiveData.addSource(first) { value ->
        firstEmit.value = value
        combine()
    }
    finalLiveData.addSource(second) { value ->
        secondEmit.value = value
        combine()
    }
    finalLiveData.addSource(third) { value ->
        thirdEmit.value = value
        combine()
    }
    return finalLiveData
}

/**
 * Wrapper that wraps an emitted value.
 */
private class Emit<T> {

    var emitted: Boolean = false

    var value: T? = null
        set(value) {
            field = value
            emitted = true
        }

    fun reset() {
        value = null
        emitted = false
    }
}